# -*- coding: utf-8 -*-
#
# BlueZ - Bluetooth protocol stack for Linux
#
# Copyright © 2012, 2015 Collabora Ltd.
#
# SPDX-License-Identifier: GPL-2.0+
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from gi.repository import GObject

from abc import ABCMeta
from abc import abstractmethod
import dbus
import dbus.service
import re
import subprocess
import time


bluetooth_profiles = {
    "0000111f-0000-1000-8000-00805f9b34fb": "HFP AG",
    "00001103-0000-1000-8000-00805f9b34fb": "DUN GW",
    "00001105-0000-1000-8000-00805f9b34fb": "OPP Server",
    "0000110a-0000-1000-8000-00805f9b34fb": "PBAP PSE",
    "0000110c-0000-1000-8000-00805f9b34fb": "AVRCP Target",
    "0000110e-0000-1000-8000-00805f9b34fb": "AVRCP Control",
    "00001112-0000-1000-8000-00805f9b34fb": "A2DP Source",
    "00001116-0000-1000-8000-00805f9b34fb": "HSP AG",
    "0000112f-0000-1000-8000-00805f9b34fb": "SAP Server",
    "0000112f-0000-1000-8000-00805f9b34fb": "PAN NAP",
    "00001132-0000-1000-8000-00805f9b34fb": "MAP MSE",
}


def bluetooth_profile_uuid_to_name(uuid):
    return bluetooth_profiles[uuid]


def bluetooth_profile_is_supported(uuid):
    return uuid in bluetooth_profiles


def TEST_START(str):
    print("TEST STARTED: %s" % str)


def TEST_FAILED(str):
    print("TEST FINISHED: %s : FAILED" % str)


def TEST_PASSED(str):
    print("TEST FINISHED: %s : PASSED" % str)


def get_hci(object_path):
    return re.search('hci[0-9]', object_path).group(0)


def build_device_path(hci, address):
    # Aiming for something like /org/bluez/hci0/dev_XX_XX_XX_XX_XX_XX
    if hci == '' or '/' in hci:
        raise Exception('Invalid hci ‘%s’' % hci)
    if address == '' or '/' in address:
        raise Exception('Invalid address ‘%s’' % address)

    hci = str(hci)
    address = str(address)

    return '/org/bluez/' + hci + '/dev_' + address.upper().replace(':', '_')


def scan_for_device(bus, adapter, device_address, result_func):
    """Scan on adapter until a device with the given device_address appears.
       Return the device object in the callback."""
    manager = dbus.Interface(bus.get_object("org.bluez", "/"),
                             "org.freedesktop.DBus.ObjectManager")
    device_obj = None
    match = None

    print('Scanning for device ‘%s’ on adapter ‘%s’…' %
          (device_address, adapter.object_path))

    # Sanity check.
    adapter_address = adapter.Get(
        'org.bluez.Adapter1',
        'Address',
        dbus_interface='org.freedesktop.DBus.Properties')
    assert(adapter_address != device_address)

    objs = manager.GetManagedObjects()
    for path, interfaces in objs.items():
        print('Saw: ' + path)

        if 'org.bluez.Device1' in interfaces and \
           interfaces['org.bluez.Device1']['Address'] == device_address and \
           interfaces['org.bluez.Device1']['Adapter'] == adapter.object_path:
            print('Found device ‘%s’' % path)
            result_func(bus.get_object('org.bluez', path))
            return

    def interfaces_added(path, interfaces):
        print('Interfaces added: ' + path)

        if 'org.bluez.Device1' not in interfaces or \
           interfaces['org.bluez.Device1']['Address'] != device_address or \
           interfaces['org.bluez.Device1']['Adapter'] != adapter.object_path:
            return

        print('Found device ‘%s’' % path)
        device_obj = bus.get_object('org.bluez', path)

        adapter.StopDiscovery(dbus_interface='org.bluez.Adapter1')
        match.remove()

        result_func(device_obj)

    match = bus.add_signal_receiver(
        interfaces_added,
        dbus_interface='org.freedesktop.DBus.ObjectManager',
        signal_name='InterfacesAdded')

    # Start discovery; ignore errors if we’re already discovering.
    try:
        adapter.StartDiscovery(dbus_interface='org.bluez.Adapter1')
    except error:
        if error.get_dbus_name() == 'org.bluez.Error.InProgress':
            pass
        raise error


# 0 is the initiator side, 1 is the responder side
# Note that if the adapters are already paired, this is a no-op and will return
# success.
def bluetooth_pair_adapters(bus, adapter0, adapter1, iocap,
                            agent_path, result_func):
    device0 = None
    device1 = None

    assert(adapter0.object_path != adapter1.object_path)

    print('Pairing adapters ‘%s’ and ‘%s’…' %
          (adapter0.object_path, adapter1.object_path))

    agent_manager_iface = dbus.Interface(bus.get_object('org.bluez',
                                                        '/org/bluez'),
                                         'org.bluez.AgentManager1')
    agent_manager_iface.RegisterAgent(agent_path, iocap)
    agent_manager_iface.RequestDefaultAgent(agent_path)

    props0 = adapter0.GetAll('org.bluez.Adapter1',
                             dbus_interface='org.freedesktop.DBus.Properties')
    props1 = adapter1.GetAll('org.bluez.Adapter1',
                             dbus_interface='org.freedesktop.DBus.Properties')

    hci0 = get_hci(adapter0.object_path)
    hci1 = get_hci(adapter1.object_path)

    def pair_finish(device0, device1):
        if device0 is not None:
            print('Paired adapters ‘%s’ and ‘%s’' %
                  (adapter0.object_path, adapter1.object_path))
        agent_manager_iface.UnregisterAgent(agent_path)
        result_func(device0, device1)

    def pair_reply():
        assert(device0 is not None and device1 is not None)
        pair_finish(device0, device1)

    def error_reply(error):
        if error.get_dbus_name() == 'org.bluez.Error.AlreadyExists':
            # No-op.
            pair_finish(device0, device1)
            return

        print("Creating device ‘%s’ failed: %s" % (device0.object_path, error))
        pair_finish(None, None)

    def scan_finish(device0, device1):
        print('Finished scanning; starting to pair…')
        device0.Pair(reply_handler=pair_reply, error_handler=error_reply,
                     timeout=60000, dbus_interface='org.bluez.Device1')

    def scan1_result(device):
        nonlocal device0, device1

        assert(device is not None)
        assert(device.object_path ==
               build_device_path(get_hci(adapter0.object_path),
                                 props1['Address']))
        device0 = device
        if device1 is not None:
            scan_finish(device0, device1)

    def scan2_result(device):
        nonlocal device0, device1

        assert(device is not None)
        assert(device.object_path ==
               build_device_path(get_hci(adapter1.object_path),
                                 props0['Address']))
        device1 = device
        if device0 is not None:
            scan_finish(device0, device1)

    scan_for_device(bus, adapter0, props1['Address'], result_func=scan1_result)
    scan_for_device(bus, adapter1, props0['Address'], result_func=scan2_result)


def object_set_properties(obj, properties_interface, properties, result_func):
    """Set one or more properties and wait for a change notification."""
    assert(len(properties) > 0)

    awaiting_notification = set()

    def properties_changed(interface_name, changed_properties,
                           invalidated_properties):
        nonlocal awaiting_notification

        if interface_name != properties_interface:
            return

        awaiting_notification = \
            awaiting_notification - changed_properties.keys() - \
            set(invalidated_properties)

        if not awaiting_notification:
            match.remove()
            result_func(obj)

    iface = dbus.Interface(obj, 'org.freedesktop.DBus.Properties')
    match = iface.connect_to_signal('PropertiesChanged', properties_changed)
    vals = iface.GetAll(properties_interface)

    # Set the properties if they weren’t already set. This is not entirely
    # race-proof.
    for p, v in properties.items():
        if vals[p] != v:
            awaiting_notification.add(p)
            iface.Set(properties_interface, p, v, signature='ssv')

    if len(awaiting_notification) == 0:
        match.remove()
        GObject.idle_add(lambda: result_func(obj))


def object_set_properties_blocking(obj, properties_interface, properties,
                                   main_loop):
    object_set_properties(obj, properties_interface, properties,
                          lambda o: main_loop.quit())
    main_loop.run()


def object_wait_for_properties(obj, properties_interface, properties,
                               result_func):
    """Wait for properties to be updated if needed"""
    assert(len(properties) > 0)

    awaiting_notification = set()

    def properties_changed(interface_name, changed_properties,
                           invalidated_properties):
        nonlocal awaiting_notification

        if interface_name != properties_interface:
            return

        awaiting_notification = \
            awaiting_notification - changed_properties.keys() - \
            set(invalidated_properties)

        if len(awaiting_notification) == 0:
            match.remove()
            result_func(obj)

    iface = dbus.Interface(obj, 'org.freedesktop.DBus.Properties')
    match = iface.connect_to_signal('PropertiesChanged', properties_changed)
    vals = iface.GetAll(properties_interface)

    for p, v in properties.items():
        if vals[p] != v:
            awaiting_notification.add(p)

    if len(awaiting_notification) == 0:
        match.remove()
        GObject.idle_add(lambda: result_func(obj))


def object_wait_for_properties_blocking(obj, properties_interface, properties,
                                        main_loop):
    object_wait_for_properties(obj, properties_interface, properties,
                               lambda o: main_loop.quit())
    main_loop.run()


def adapters_ensure_powered(adapters, result_func=None):
    assert(len(adapters) > 0)
    remaining = set(adapters)

    def inner_result_func(obj):
        nonlocal remaining

        print('Adapter ‘%s’ is powered' % obj.object_path)
        remaining.remove(obj)
        if not remaining and result_func:
            result_func()

    for adapter in adapters:
        print('Making adapter ‘%s’ powered…' % adapter.object_path)
        object_set_properties(adapter, 'org.bluez.Adapter1', {
            'Powered': True
        }, inner_result_func)


def adapters_make_pairable(adapters, result_func=None):
    assert(len(adapters) > 0)
    remaining = set(adapters)

    def inner_result_func(obj):
        nonlocal remaining

        print('Adapter ‘%s’ is pairable' % obj.object_path)
        remaining.remove(obj)
        if not remaining and result_func:
            result_func()

    adapters_ensure_powered(adapters)

    for adapter in adapters:
        print('Making adapter ‘%s’ pairable…' % adapter.object_path)
        object_set_properties(adapter, 'org.bluez.Adapter1', {
            'Pairable': True,
            'Discoverable': True,
        }, inner_result_func)


def adapters_make_discoverable(adapters, result_func=None):
    assert(len(adapters) > 0)
    remaining = set(adapters)

    def inner_result_func(obj):
        nonlocal remaining

        print('Adapter ‘%s’ is discoverable' % obj.object_path)
        remaining.remove(obj)
        if not remaining and result_func:
            result_func()

    for adapter in adapters:
        print('Making adapter ‘%s’ discoverable…' % adapter.object_path)
        object_set_properties(adapter, 'org.bluez.Adapter1', {
            'Powered': True,
            'Discoverable': True,
        }, inner_result_func)


class Rejected(dbus.DBusException):
    _dbus_error_name = 'org.bluez.Error.Rejected'


class ConstantAgent(dbus.service.Object):
    """Agent which returns 0000 for all PINs and passkeys."""
    exit_on_release = False

    def __init__(self, bus, object_path, main_loop):
        self.__main_loop = main_loop
        super().__init__(bus, object_path)

    def set_exit_on_release(self, exit_on_release):
        self.exit_on_release = exit_on_release

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="", out_signature="")
    def Release(self):
        if self.exit_on_release:
            self.__main_loop.quit()

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="os", out_signature="")
    def AuthorizeService(self, device, uuid):
        print("Authorize (%s, %s)" % (device, uuid))

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="o", out_signature="s")
    def RequestPinCode(self, device):
        return '0000'

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="os", out_signature="")
    def DisplayPinCode(self, device, pincode):
        print("DisplayPinCode (%s, %s)" % (device, pincode))

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="o", out_signature="u")
    def RequestPasskey(self, device):
        return dbus.UInt32(0)

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="ouq", out_signature="")
    def DisplayPasskey(self, device, passkey, entered):
        print("DisplayPasskey (%s, %06d, %d)" % (device, passkey, entered))

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="o", out_signature="")
    def RequestAuthorization(self, device):
        print('RequestAuthorization (%s)' % device)

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="ou", out_signature="")
    def RequestConfirmation(self, device, passkey):
        print('RequestConfirmation (%s, %06d)' % (device, passkey))

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="", out_signature="")
    def Cancel(self):
        print("Cancel")


class AskAgent(dbus.service.Object):
    """Agent which asks about PINs and passkeys."""
    exit_on_release = True

    def __init__(self, bus, object_path, main_loop,
                 authorize_everything=False):
        self.__main_loop = main_loop
        self.__authorize_everything = authorize_everything
        super().__init__(bus, object_path)

    def set_exit_on_release(self, exit_on_release):
        self.exit_on_release = exit_on_release

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="", out_signature="")
    def Release(self):
        print("Release")
        if self.exit_on_release:
            self.__main_loop.quit()

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="os", out_signature="")
    def AuthorizeService(self, device, uuid):
        if self.__authorize_everything:
            print("Authorizing %s, %s" % (device, uuid))
        else:
            print("Authorize (%s, %s)" % (device, uuid))
            authorize = input("Authorize connection (yes/no): ")
            if (authorize == "yes"):
                return
            raise Rejected("Connection rejected by user")

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="o", out_signature="s")
    def RequestPinCode(self, device):
        print("RequestPinCode (%s)" % (device))
        return input("Enter PIN Code: ")

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="os", out_signature="")
    def DisplayPinCode(self, device, pincode):
        print("DisplayPinCode (%s, %s)" % (device, pincode))

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="o", out_signature="u")
    def RequestPasskey(self, device):
        print("RequestPasskey (%s)" % (device))
        passkey = input("Enter passkey: ")
        return dbus.UInt32(passkey)

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="ouq", out_signature="")
    def DisplayPasskey(self, device, passkey, entered):
        print("DisplayPasskey (%s, %06d, %d)" % (device, passkey, entered))

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="o", out_signature="")
    def RequestAuthorization(self, device):
        if self.__authorize_everything:
            print("Confiming authorization (%s)" % (device))
        else:
            print("RequestAuthorization (%s)" % (device, passkey))
            confirm = input("Confirm authorization (yes/no): ")
            if (confirm == "yes"):
                return
            raise Rejected("Authorization rejected by user")

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="ou", out_signature="")
    def RequestConfirmation(self, device, passkey):
        if self.__authorize_everything:
            print("Confiming passkey %06d (%s)" % (passkey, device))
        else:
            print("RequestConfirmation (%s, %06d)" % (device, passkey))
            confirm = input("Confirm passkey (yes/no): ")
            if (confirm == "yes"):
                return
            raise Rejected("Passkey doesn't match")

    @dbus.service.method("org.bluez.Agent1",
                         in_signature="", out_signature="")
    def Cancel(self):
        print("Cancel")


class AdapterDeviceTester():
    """Adapter tester class which tests pairing with a device (a phone) in both
       directions — as the initiator and then as the responder. After pairing,
       it runs the tests in the device_test_class on the paired device."""
    def __init__(self, bus, adapter, device, device_test_class):
        self.__bus = bus
        self.__adapter_obj = adapter
        self.__device_obj = device
        self.__device_test_class = device_test_class

    def __pair_reply(self):
        self.__adapter_obj.RemoveDevice(
            self.__device_obj.object_path,
            dbus_interface='org.bluez.Adapter1')
        self.__pairing_initiator_result_func(None)

    def __pair_error(self, error):
        if error.get_dbus_name() == 'org.bluez.Error.AlreadyExists':
            self.__pairing_initiator_result_func(None)
        else:
            self.__pairing_initiator_result_func(
                "Pairing Initiator: Create device failed %s" % (error))

    def _create_paired_device(self, device_obj):
        device_obj.Pair(dbus_interface='org.bluez.Device1',
                        reply_handler=self.__pair_reply,
                        error_handler=self.__pair_error)

    def __show_menu(self):
        self.__adapter_obj.StopDiscovery(dbus_interface='org.bluez.Adapter1')

        manager = dbus.Interface(self.__bus.get_object('org.bluez', '/'),
                                 'org.freedesktop.DBus.ObjectManager')
        objects = manager.GetManagedObjects()

        print('Devices:')
        for p, i in objects.items():
            if 'org.bluez.Device1' in i and \
               i['org.bluez.Device1']['Adapter'] == \
               self.__adapter_obj.object_path:
                address = str(i['org.bluez.Device1']['Address'])
                name = str(i['org.bluez.Device1'].get('Name', address))

                print('%s: %s, %s' % (p, address, name))
        device_path = input("Select one device to pair with: ")

        if device_path == '':
            ret = input("Retry discovery (y/n) ")
            if ret == 'y' or ret == 'Y':
                self.start_pairing_initiator(
                    self.__pairing_initiator_result_func)
                return
            self.__pairing_initiator_result_func(
                "Pairing Initiator: No device selected")
            return

        if device_path not in objects:
            self.__pairing_initiator_result_func(
                "Pairing Initiator: Invalid device selected")
            return

        self.__device_obj = self.__bus.get_object('org.bluez', device_path)
        self._create_paired_device(self.__device_obj)

    def get_device(self):
        self.__device_test_class(self.__device_obj)

    def start_pairing_initiator(self, result_func):
        self.__pairing_initiator_result_func = result_func

        if self.__device_obj:
            self._create_paired_device(self.__device_obj)
            return

        # Unpair from everything first so we start with a clean slate.
        manager = dbus.Interface(self.__bus.get_object('org.bluez', '/'),
                                 'org.freedesktop.DBus.ObjectManager')
        objects = manager.GetManagedObjects()

        for p, i in objects.items():
            if 'org.bluez.Device1' in i and \
               i['org.bluez.Device1']['Adapter'] == \
               self.__adapter_obj.object_path and \
               i['org.bluez.Device1']['Paired']:
                self.__adapter_obj.RemoveDevice(
                    p,
                    dbus_interface='org.bluez.Adapter1')

        def properties_cb(obj):
            self.__adapter_obj.StartDiscovery(
                dbus_interface='org.bluez.Adapter1')
            GObject.timeout_add(12000, self.__show_menu)

        object_set_properties(self.__adapter_obj, 'org.bluez.Adapter1', {
            'Powered': True,
            'Alias': 'Bluez',
            'Pairable': False,
        }, properties_cb)

    def __properties_changed_responder(self, interface_name,
                                       changed_properties,
                                       invalidated_properties,
                                       path):
        if interface_name != 'org.bluez.Device1':
            return
        if 'Paired' not in changed_properties.keys() and \
           'Paired' not in invalidated_properties:
            return

        self.__responder_match.remove()
        self.__responder_match = None

        device_obj = self.__bus.get_object('org.bluez', path)
        self.__device_test_class(device_obj)
        self.__pairing_responder_result_func(None)

    def start_pairing_responder(self, result_func):
        self.__pairing_responder_result_func = result_func

        adapter_props = dbus.Interface(self.__adapter_obj,
                                       'org.freedesktop.DBus.Properties')
        props = adapter_props.GetAll('org.bluez.Adapter1')

        print("In the phone start pairing with %s (%s)." %
              (props['Alias'], props["Address"]))

        # Wait for any Bluetooth device to become paired.
        self.__responder_match = self.__bus.add_signal_receiver(
            self.__properties_changed_responder,
            dbus_interface='org.freedesktop.DBus.Properties',
            signal_name='PropertiesChanged',
            path_keyword='path')

        def properties_cb(obj):
            pass

        object_set_properties(self.__adapter_obj, 'org.bluez.Adapter1', {
            'Powered': True,
            'Alias': 'BlueZ',
            'Discoverable': True,
            'Pairable': True,
        }, properties_cb)


class DeviceProfileTester(metaclass=ABCMeta):
    """Device tester class which runs a test function for each profile
       advertised by a device. The device can either be specified or discovered
       and paired with by the class, interactively."""
    def __init__(self, device):
        self._device_obj = dbus.Interface(device, "org.bluez.Device1")

        self.__match = self._device_obj.connect_to_signal(
            'PropertiesChanged',
            self.__device_properties_changed)

        paired = self._device_obj.Get(
            'org.bluez.Device1',
            'Paired',
            dbus_interface='org.freedesktop.DBus.Properties')
        if paired:
            self.__handle_is_paired()

    def __handle_is_paired(self):
        self._device_obj.Set('org.bluez.Device1', "Trusted", True,
                             dbus_interface='org.freedesktop.DBus.Properties')
        self.__match.remove()
        self.__match = None

        uuids = self._device_obj.Get(
            'org.bluez.Device1',
            'UUIDs',
            dbus_interface='org.freedesktop.DBus.Properties')
        self.__parse_uuids(uuids)

    def __parse_uuids(self, uuids):
        profiles = []

        print("Profiles supported:")
        for uuid in uuids:
            try:
                name = bluetooth_profile_uuid_to_name(uuid)
                print(" • %s" % name)
                profiles.append(name)
            except KeyError:
                pass

        self.device_test_profiles(profiles)

    def __device_properties_changed(self, iface_name, changed_properties,
                                    invalidated_properties):
        if iface_name != 'org.bluez.Device1':
            return

        if 'Paired' in changed_properties and changed_properties['Paired']:
            self.__handle_is_paired()

    @abstractmethod
    def device_test_profiles(self, profiles):
        pass
