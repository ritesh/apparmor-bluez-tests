# -*- coding: utf-8 -*-

# Copyright © 2015 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import hashlib
import os

from gi.repository import GLib
from gi.repository import Gio


class TumblerMixin:
    def __init__(self):
        # Monitor thumbnail creation to know when it's done. The DBus API
        # doesn't have a method to query the initial state but we can be
        # reasonably sure it's not thumbnailing anything at this point.
        self.tumbler_queue = []
        self.tumbler = Gio.DBusProxy.new_for_bus_sync(
            Gio.BusType.SESSION,
            Gio.DBusProxyFlags.DO_NOT_LOAD_PROPERTIES,
            None,
            'org.freedesktop.thumbnails.Thumbnailer1',
            '/org/freedesktop/thumbnails/Thumbnailer1',
            'org.freedesktop.thumbnails.Thumbnailer1',
            None)
        self.tumbler.connect('g-signal', self.__g_signal_cb)

        # Track pending errors. Tumbler emits three signals which we care
        # about:
        #  - Ready, which indicates successful thumbnailing for some URIs
        #  - Error, which indicates errors in thumbnailing for some URIs
        #  - Finished, which is the final signal emitted for a request
        # Ready and Error may both be emitted for the same URI, for example if
        # one of the thumbnailers encounters an error when thumbnailing that
        # URI, but another thumbnailer subsequently succeeds.
        # Therefore we must track errors and only propagate them when Finished
        # is received.
        #
        # Each element of this list of a tuple: (uri, error_message).
        self.tumbler_errors = []

        # Spy on unicast signals that weren't meant for us, because
        # the Thumbnailer API uses those, and we want to use them to
        # determine when it has finished. GDBus doesn't have
        # high-level API for this sort of nonsense so we do it the hard way.
        match_rule = ("type=signal,"
                      "sender='org.freedesktop.thumbnails.Thumbnailer1',"
                      "eavesdrop=true")
        conn = self.tumbler.get_connection()
        conn.call_sync('org.freedesktop.DBus',
                       '/org/freedesktop/DBus',
                       'org.freedesktop.DBus',
                       'AddMatch',
                       GLib.Variant('(s)', (match_rule,)),
                       None, Gio.DBusCallFlags.NONE, -1, None)

    def __g_signal_cb(self, proxy, sender_name, signal_name, parameters):
        print('TumblerMixin: Received signal:', signal_name)

        if signal_name == 'Started':
            child = parameters.get_child_value(0)
            self.tumbler_queue.append(child.get_uint32())
        elif signal_name == 'Finished':
            child = parameters.get_child_value(0)
            self.tumbler_queue.remove(child.get_uint32())

            # Any errors remaining?
            if len(self.tumbler_errors) > 0:
                l = [': '.join(p) for p in self.tumbler_errors]
                raise Exception("Error creating thumbnails: %s" % ', '.join(l))

            # Clear them.
            self.tumbler_errors = []
        elif signal_name == 'Ready':
            child = parameters.get_child_value(1)
            uris = child.get_strv()

            # Remove any now-successful URIs from the error list.
            self.tumbler_errors = [p for p in self.tumbler_errors
                                   if p[0] not in uris]
        elif signal_name == 'Error':
            child = parameters.get_child_value(1)
            uris = child.get_strv()
            child = parameters.get_child_value(3)
            msg = child.get_string()

            # Append to the error list.
            for uri in uris:
                self.tumbler_errors.append((uri, msg))

    def __get_supported_cb(self, source, result):
        self.tumbler.call_finish(result)
        self.loop.quit()

    def tumbler_drain_queue(self):
        # Drain the DBus queue to make sure we received all signals
        self.tumbler.call('GetSupported',
                          GLib.Variant.new_tuple(),
                          Gio.DBusCallFlags.NONE,
                          -1,
                          None,
                          self.__get_supported_cb)
        self.loop.run()

        context = self.loop.get_context()
        while len(self.tumbler_queue) > 0:
            context.iteration(True)

    def tumbler_assert_thumbnailed(self, root, filename):
        removable = False
        monitor = Gio.VolumeMonitor.get()
        mounts = monitor.get_mounts()
        for m in mounts:
            if m.get_root().get_path() == root:
                removable = m.can_eject()
                break

        media_path = os.path.join(root, filename)

        if removable:
            dirname = os.path.dirname(media_path)
            basename = os.path.basename(media_path)
            digest = hashlib.md5(bytes(basename, encoding='UTF-8')).hexdigest()
            thumbdir = os.path.join(dirname, '.sh_thumbnails')
        else:
            uri = 'file://' + media_path
            digest = hashlib.md5(bytes(uri, encoding='UTF-8')).hexdigest()
            thumbdir = os.path.join(self.homedir, '.cache', 'thumbnails')

        path = os.path.join(thumbdir, 'normal', digest + '.png')
        self.assertTrue(os.path.isfile(path))
