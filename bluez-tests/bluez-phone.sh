#!/bin/sh

set -e

hcitool scan
echo "Enter the device you want to pair with:"
read address
/usr/lib/chaiwala-tests/bluez/ubt -i hci0 -d $address
